<?php
	include_once("../../../vendor/autoload.php");
	
	use Imran\BITM\SEIP106357\Hobbies\CheckboxMultiple;
	use Imran\BITM\SEIP106357\Utility\Utility;
	
	$myHobbies = new CheckboxMultiple();
	$myHobby = $myHobbies->show($_REQUEST['id']);
	
	
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Hobby</title>
	<link rel="stylesheet" href="css/style.css" media="screen" title="no title" charset="utf-8">
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div id="wrapper">
		<div id="header" class="page-header">
			<h1><a href="index.php">Hobby</a></h1>
		</div><!-- #header -->
		<div id="jumbotron" class="jumbotron">
		  <div class="container">
			<div class="add-gender-sub-container">
				<h2>My Hobbies</h2>
				<h3><?php echo $myHobby->title;?></h3>
				<table class="table table-condensed">
							<tr class="active">
								<td><?php echo $myHobby->Football;?></td>
								<td><?php echo $myHobby->Cricket;?></td>		
								<td><?php echo $myHobby->TV;?></td>
							</tr>
							<tr class="success">
								<td><?php echo $myHobby->Family_Time;?></td>
								<td><?php echo $myHobby->Fishing;?></td>		
								<td><?php echo $myHobby->Computer;?></td>
							</tr>
							<tr class="active">
								<td><?php echo $myHobby->Hunting;?></td>
								<td><?php echo $myHobby->Traveling;?></td>		
								<td><?php echo $myHobby->Shopping;?></td>
							</tr>
							<tr class="success">
								<td><?php echo $myHobby->Sleeping;?></td>
								<td><?php echo $myHobby->Chees;?></td>		
								<td><?php echo $myHobby->Music;?></td>
							</tr>
						</table>
			</div>
		  </div>
		</div>
		<div id="footer" class="page-header">
			<p>
				&copy; Mohammad Emran Kabir. SEID-106357. PHP Batch-11
			</p>
		</div><!-- #footer -->
	</div><!-- #wrapper -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>
