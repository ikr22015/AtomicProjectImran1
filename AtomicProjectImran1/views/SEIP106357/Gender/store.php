<?php
	include_once("../../../vendor/autoload.php");
	
	use Imran\BITM\SEIP106357\Gender\Radio;
	use Imran\BITM\SEIP106357\Utility\Utility;
	
	$gender = new Radio($_POST);
	$gender->store();
?>