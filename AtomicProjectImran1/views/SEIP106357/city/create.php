<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>City</title>
	<link rel="stylesheet" href="../../../resource/css/style.css" media="screen" title="no title" charset="utf-8">
    <!-- Bootstrap -->
    <link href="../../../resource/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div id="wrapper">
		<div id="header" class="page-header">
			<h1><a href="index.php">City</a></h1>
			<nav class="navbar navbar-inverse">
				<ul>
					<li><a class="active" href="index.php">Home</a></li>
					<li><a href="../../../index.php">Go to Ptoject Navigation Page</a></li>
					<li><a href="#">Click here to view the source code</a></li>
				</ul>
			</nav>
		</div><!-- #header -->
		<div id="jumbotron" class="jumbotron">
		  <div class="container">
			<div class="city-add-sub-container">
				<h2>Add Your City</h2>
				<div class="form-group">
					<form action="store.php" method="post">
						<label>Enter Your Name</label>
						<input style="padding-left:5px;margin-left:7px" 
								type="text" 
								autofocus = "autofocus" 
								required="required""
								name="name" 
								placeholder="Name" 
								value="" /></br>
						<label for="sell">Which city do you live in?</label></br>
						<select class="form-control" name="city" id="sel1">
							<option>Dhaka</option>
							<option>Sylhet</option>
							<option>Rajshahi</option>
							<option>Chittagong</option>
							<option>Khulna</option>
							<option>Borishal</option>
							<option>Joshor</option>
						</select>
						<button style="margin-top:10px" type="submit" name="button" value="">Submit</button>
						<input type="reset" name="" value="Reset"/>
					</form>
				</div>
			</div>
		  </div>
		</div>
		<div id="footer" class="page-header">
			<p>
				&copy; Mohammad Emran Kabir. SEID-106357. PHP Batch-11
			</p>
		</div><!-- #footer -->
	</div><!-- #wrapper -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>
