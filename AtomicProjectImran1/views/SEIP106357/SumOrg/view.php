<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Summary of Organization</title>
	<link rel="stylesheet" href="css/style.css" media="screen" title="no title" charset="utf-8">
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div id="wrapper">
		<div id="header" class="page-header">
			<h1><a href="index.php">Summary of Organization</a></h1>
		</div><!-- #header -->
		<div id="jumbotron" class="jumbotron">
		  <div class="container">
			<div class="profile-sum-org-sub-container">
				<div class="panel panel-success">
					<div class="panel-heading">Organization Summary</div>
						<div class="panel-body">
							<p>Maybe there's now good reason to re-introduce this old technique – JUST for our small screen layouts? Our wide screens won't change, but media queries allow us to use those paragraph breaks more usefully on mobile view only.</br>
							
							This lets us maintain the familiar breathing space on larger screens but potentially provide more readability and a better UX on small screens. It won’t be dramatic, but it may be enough to put three paragraphs onscreen, rather than two.</br>
							
							This lets us maintain the familiar breathing space on larger screens but potentially provide more readability and a better UX on small screens. It won’t be dramatic, but it may be enough to put three paragraphs onscreen, rather than two.</br>
							
							This lets us maintain the familiar breathing space on larger screens but potentially provide more readability and a better UX on small screens. It won’t be dramatic, but it may be enough to put three paragraphs onscreen, rather than two.</br>
							
							This lets us maintain the familiar breathing space on larger screens but potentially provide more readability and a better UX on small screens. It won’t be dramatic, but it may be enough to put three paragraphs onscreen, rather than two.</br>
							</p>
						</div>
				</div>
			</div>
		  </div>
		</div>
		<div id="footer" class="page-header">
			<p>
				&copy; Mohammad Emran Kabir. SEID-106357. PHP Batch-11
			</p>
		</div><!-- #footer -->
	</div><!-- #wrapper -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>
