-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 01, 2016 at 06:54 PM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `imran`
--

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE IF NOT EXISTS `cities` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `title`, `location`) VALUES
(1, 'Imran Kabir', 'Sylhet'),
(2, 'Imran Kabir', 'Sylhet'),
(3, 'Imran Kabir', 'Sylhet'),
(4, 'Imran Kabir', 'Sylhet'),
(5, 'Imran Kabir', 'Sylhet'),
(6, 'Shumee', ''),
(7, 'Shumee', ''),
(8, 'Shumee', 'Sylhet'),
(9, 'Shumee', 'Sylhet'),
(10, 'Nafee', 'Chittagong'),
(11, 'Nafee', 'Chittagong'),
(12, 'Nafee', 'Chittagong'),
(13, 'Nafee', 'Chittagong'),
(14, 'Nafee', 'Chittagong'),
(15, 'Nafee', 'Chittagong'),
(16, 'Imran Kabir', 'Rajshahi'),
(17, 'Imran Kabir', 'Rajshahi'),
(18, 'Imran Kabir', 'Khulna'),
(19, 'Nafee', 'Chittagong'),
(20, 'Rajib', 'Sylhet');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
