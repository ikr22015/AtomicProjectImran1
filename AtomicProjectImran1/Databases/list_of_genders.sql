-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 01, 2016 at 06:54 PM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `imran`
--

-- --------------------------------------------------------

--
-- Table structure for table `list_of_genders`
--

CREATE TABLE IF NOT EXISTS `list_of_genders` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `list_of_genders`
--

INSERT INTO `list_of_genders` (`id`, `title`, `name`) VALUES
(1, 'Male', 'Imran Kabir'),
(2, 'Male', 'Imran Kabir'),
(3, 'Male', 'Imran Kabir'),
(4, 'Male', 'Imran Kabir'),
(5, 'Female', 'PHP7<br>'),
(6, 'Male', 'Imran Kabir'),
(7, 'Male', 'Imran Kabir'),
(8, 'Male', 'Imran Kabir'),
(9, 'Male', 'Imran Kabir'),
(10, 'Male', 'Imran Kabir'),
(11, 'Male', 'Mahmudur Rahman'),
(12, 'Female', 'Amy Blackwood'),
(13, 'Male', 'Denial Clarck'),
(14, 'Female', 'Jomila Begum');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `list_of_genders`
--
ALTER TABLE `list_of_genders`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `list_of_genders`
--
ALTER TABLE `list_of_genders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
